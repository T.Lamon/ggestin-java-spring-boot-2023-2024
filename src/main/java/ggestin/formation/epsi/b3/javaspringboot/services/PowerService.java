package ggestin.formation.epsi.b3.javaspringboot.services;

import ggestin.formation.epsi.b3.javaspringboot.model.Power;

import java.util.List;
import java.util.Optional;

public interface PowerService {

    Power savePower(Power power);

    boolean existsById(Long id);

    List<Power> findAll();

    void deleteById(Long id);

    Optional<Power> findById(Long id);
}
