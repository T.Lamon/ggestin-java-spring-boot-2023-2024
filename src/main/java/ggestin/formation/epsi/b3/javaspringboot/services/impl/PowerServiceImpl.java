package ggestin.formation.epsi.b3.javaspringboot.services.impl;

import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import ggestin.formation.epsi.b3.javaspringboot.repositories.PowerRepository;
import ggestin.formation.epsi.b3.javaspringboot.services.PowerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PowerServiceImpl implements PowerService {

    private final PowerRepository powerRepository;

    @Override
    public Power savePower(Power power) {
        return this.powerRepository.save(power);
    }

    @Override
    public boolean existsById(Long id) {
        return this.powerRepository.existsById(id);
    }

    @Override
    public List<Power> findAll() {
        return this.powerRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        this.powerRepository.deleteById(id);
    }

    @Override
    public Optional<Power> findById(Long id) {
        return this.powerRepository.findById(id);
    }
}
