package ggestin.formation.epsi.b3.javaspringboot.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class PowerDTO {
    private Long id;
    private String name;
    private String description;
}
