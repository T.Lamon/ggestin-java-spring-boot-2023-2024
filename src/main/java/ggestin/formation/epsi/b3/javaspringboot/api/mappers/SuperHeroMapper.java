package ggestin.formation.epsi.b3.javaspringboot.api.mappers;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.SuperHeroDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.SuperHeroMinimalDTO;
import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = {PowerMapper.class, VillainMapper.class})
abstract public class SuperHeroMapper {

    @Mapping(target = "nameSuperHero", source = "name")
    public abstract SuperHeroMinimalDTO toMinimalDto(SuperHero entity);

    public abstract List<SuperHeroMinimalDTO> toMinimalDtos(List<SuperHero> entities);

    public abstract SuperHeroDTO toDto(SuperHero entity);

    public abstract List<SuperHeroDTO> toDtos(List<SuperHero> entities);

    List<String> toNames(List<Power> powers) {
        return powers.stream().map(Power::getName).toList();
    }
}
