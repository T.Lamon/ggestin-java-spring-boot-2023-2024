package ggestin.formation.epsi.b3.javaspringboot.api;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.VillainCreateDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.VillainDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.mappers.VillainMapper;
import ggestin.formation.epsi.b3.javaspringboot.model.Villain;
import ggestin.formation.epsi.b3.javaspringboot.services.VillainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/villains")
@RequiredArgsConstructor
@Slf4j
public class VillainController {

    private final VillainService villainService;
    private final VillainMapper villainMapper;

    @GetMapping()
    public ResponseEntity<List<VillainDTO>> getAll(
    ) {
        return ResponseEntity.ok(this.villainMapper.toDtos(this.villainService.findAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<VillainDTO> getById(
            @PathVariable() Long id
    ) {
        return ResponseEntity.of(
                this.villainService.findById(id).map(this.villainMapper::toDto)
        );
    }

    @PostMapping()
    public ResponseEntity<VillainDTO> createVillain(
            @RequestBody VillainCreateDTO villainCreateDTO
    ) {
        Villain createdVillain = villainService.saveVillain(
                this.villainMapper.fromDto(villainCreateDTO)
        );

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(this.villainMapper.toDto(createdVillain));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VillainDTO> updateVillain(
            @PathVariable() Long id,
            @RequestBody VillainDTO villainDTO
    ) {
        if (!id.equals(villainDTO.getId())) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .build();
        }

        if (this.villainService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ;

        Villain updatedVillain = villainService.saveVillain(
                this.villainMapper.fromDto(villainDTO)
        );
        return ResponseEntity
                .ok(this.villainMapper.toDto(updatedVillain));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteVillain(
            @PathVariable() Long id
    ) {
        if (!this.villainService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }
        ;

        this.villainService.deleteById(id);

        if (this.villainService.existsById(id)) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }

        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .build();
    }
}
