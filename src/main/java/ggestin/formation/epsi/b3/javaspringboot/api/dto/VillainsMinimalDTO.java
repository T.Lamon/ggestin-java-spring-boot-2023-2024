package ggestin.formation.epsi.b3.javaspringboot.api.dto;


import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class VillainsMinimalDTO {
    private Long id;
    private String name;
}
