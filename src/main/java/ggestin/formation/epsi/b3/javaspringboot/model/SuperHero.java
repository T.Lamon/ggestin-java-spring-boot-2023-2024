package ggestin.formation.epsi.b3.javaspringboot.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name="superheroes")
public class SuperHero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String alterEgo;

    @ManyToMany
    private List<Power> powers = new ArrayList<>();

    @ManyToMany
    private List<Villain> villains = new ArrayList<>();

    @OneToOne
    private Villain nemesis;

    @ManyToOne
    private SuperHero mentor;

    @OneToMany(mappedBy = "mentor")
    private List<SuperHero> sidekicks = new ArrayList<>();
}
