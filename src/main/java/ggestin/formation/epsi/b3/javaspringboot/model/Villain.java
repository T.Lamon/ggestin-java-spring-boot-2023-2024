package ggestin.formation.epsi.b3.javaspringboot.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
public class Villain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 50)
    private String name;

    @ManyToMany
    private List<Power> powers;

    @ManyToMany(mappedBy = "villains")
    private List<SuperHero> superHeroes = new ArrayList<>();

    @OneToOne
    private SuperHero nemesis;
}
