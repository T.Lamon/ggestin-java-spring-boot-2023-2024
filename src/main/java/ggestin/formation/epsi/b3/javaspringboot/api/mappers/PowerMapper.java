package ggestin.formation.epsi.b3.javaspringboot.api.mappers;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.PowerCreateDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.PowerDTO;
import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PowerMapper {

    Power fromDto(PowerCreateDTO dto);

    Power fromDto(PowerDTO dto);

    PowerDTO toDto(Power power);

    List<PowerDTO> toDtos(List<Power> powers);
}
