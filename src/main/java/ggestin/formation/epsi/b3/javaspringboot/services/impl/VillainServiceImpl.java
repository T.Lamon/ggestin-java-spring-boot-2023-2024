package ggestin.formation.epsi.b3.javaspringboot.services.impl;

import ggestin.formation.epsi.b3.javaspringboot.model.Villain;
import ggestin.formation.epsi.b3.javaspringboot.repositories.VillainRepository;
import ggestin.formation.epsi.b3.javaspringboot.services.VillainService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class VillainServiceImpl implements VillainService {

    private final VillainRepository villainRepository;

    @Override
    public Villain saveVillain(Villain villain) {
        return this.villainRepository.save(villain);
    }

    @Override
    public boolean existsById(Long id) {
        return this.villainRepository.existsById(id);
    }

    @Override
    public List<Villain> findAll() {
        return this.villainRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        this.villainRepository.deleteById(id);
    }

    @Override
    public Optional<Villain> findById(Long id) {
        return this.villainRepository.findById(id);
    }
}
