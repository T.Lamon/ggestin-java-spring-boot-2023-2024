package ggestin.formation.epsi.b3.javaspringboot.api.dto;


import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SuperHeroMinimalDTO {
    private Integer id;
    private String nameSuperHero;
    private String alterEgo;
    private List<PowerDTO> powers;
}
