package ggestin.formation.epsi.b3.javaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VillainCreateDTO {
    private String name;
    private String description;
}
