package ggestin.formation.epsi.b3.javaspringboot.api;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.SuperHeroDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.SuperHeroMinimalDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.VillainsMinimalDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.mappers.PowerMapper;
import ggestin.formation.epsi.b3.javaspringboot.api.mappers.SuperHeroMapper;
import ggestin.formation.epsi.b3.javaspringboot.model.Power;
import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;
import ggestin.formation.epsi.b3.javaspringboot.model.Villain;
import ggestin.formation.epsi.b3.javaspringboot.services.SuperHeroService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/superheroes")
@RequiredArgsConstructor
@Slf4j
public class SuperHeroController {

    private final SuperHeroService superHeroService;
    private final SuperHeroMapper superHeroMapper;

    @GetMapping
    public ResponseEntity<List<SuperHeroMinimalDTO>> getSuperHeroes() {
        return ResponseEntity.ok(superHeroMapper.toMinimalDtos(superHeroService.getSuperHeroes()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<SuperHeroDTO> getSuperHeroById(@PathVariable() Integer id) {
        log.atInfo().log("getSuperHeroById {}", id);
        return ResponseEntity.of(
                superHeroService.getSuperHeroById(id).map(this.superHeroMapper::toDto)
        );
    }

    private VillainsMinimalDTO mapToVillainMinimalDto(Villain villain) {
        if (villain == null) {
            return null;
        }
        return VillainsMinimalDTO.builder()
                .id(villain.getId())
                .name(villain.getName())
                .build();
    }

    private List<VillainsMinimalDTO> mapToVillainMinimalDtos(List<Villain> villains) {
        return villains.stream().map(this::mapToVillainMinimalDto).toList();
    }
}
