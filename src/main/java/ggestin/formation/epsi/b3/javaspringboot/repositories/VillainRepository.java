package ggestin.formation.epsi.b3.javaspringboot.repositories;

import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;
import ggestin.formation.epsi.b3.javaspringboot.model.Villain;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VillainRepository
        extends
        JpaRepository<Villain, Long> {
}
