package ggestin.formation.epsi.b3.javaspringboot.api.mappers;

import ggestin.formation.epsi.b3.javaspringboot.api.dto.VillainCreateDTO;
import ggestin.formation.epsi.b3.javaspringboot.api.dto.VillainDTO;
import ggestin.formation.epsi.b3.javaspringboot.model.Villain;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface VillainMapper {

    Villain fromDto(VillainCreateDTO dto);

    Villain fromDto(VillainDTO dto);

    VillainDTO toDto(Villain villain);

    List<VillainDTO> toDtos(List<Villain> villains);

}
