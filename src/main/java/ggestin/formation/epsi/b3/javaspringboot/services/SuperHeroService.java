package ggestin.formation.epsi.b3.javaspringboot.services;

import ggestin.formation.epsi.b3.javaspringboot.model.SuperHero;

import java.util.List;
import java.util.Optional;

public interface SuperHeroService {

    List<SuperHero> getSuperHeroes();

    Optional<SuperHero> getSuperHeroById(Integer id);

}
