package ggestin.formation.epsi.b3.javaspringboot.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class SuperHeroDTO {
    private Integer id;
    private String name;
    private String alterEgo;
    private List<String> powers;
    private SuperHeroMinimalDTO mentor;
    private List<SuperHeroMinimalDTO> sidekicks;
    private List<VillainsMinimalDTO> villains;
    private VillainsMinimalDTO nemesis;
}
